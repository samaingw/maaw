#![warn(clippy::all, clippy::pedantic)]

use serde_derive::{Deserialize, Serialize};

use std::env;
use std::fs;
use std::result;

use std::collections::HashMap;
use std::collections::HashSet;
use std::iter::FromIterator;
use std::process::Command;

#[derive(Deserialize, Serialize, Debug)]
struct TagContent {
    nodes: Vec<String>,
    views: Vec<String>,
}

#[derive(Deserialize, Serialize, Debug)]
struct MaawData {
    tags: HashMap<String, TagContent>,
    nodes: HashMap<String, Vec<String>>,
    views: HashMap<String, Vec<String>>,
    current_view: String,
}

#[derive(Debug)]
enum MaawError {
    InvalidJson(serde_json::Error),
    InvalidTarget,
    InvalidCommand,
    TargetExists,
    WouldBeEmpty,
}

impl From<serde_json::Error> for MaawError {
    fn from(err: serde_json::Error) -> Self {
        MaawError::InvalidJson(err)
    }
}

struct MaawReturn {
    new_session: MaawData,
    affected_nodes: Option<Vec<String>>,
}

fn hashset(data: &[String]) -> HashSet<String> {
    HashSet::from_iter(data.iter().cloned())
}

fn maaw_tag_add(mut session: MaawData, tag: &str) -> result::Result<MaawReturn, MaawError> {
    if session.tags.contains_key(tag) {
        return Err(MaawError::TargetExists);
    }
    session.tags.insert(
        tag.to_string(),
        TagContent {
            nodes: Vec::new(),
            views: Vec::new(),
        },
    );
    Ok(MaawReturn {
        new_session: session,
        affected_nodes: None,
    })
}

fn seek_and_destroy_elem(str_arr: &mut Vec<String>, elem: &str) -> Result<(), MaawError> {
    match str_arr.iter().position(|item| item == elem) {
        Some(pos) => {
            str_arr.swap_remove(pos);
            Ok(())
        },
        None => Err(MaawError::InvalidTarget),
    }
}

fn maaw_tag_del(mut session: MaawData, tag: &str) -> result::Result<MaawReturn, MaawError> {
    if !session.tags.contains_key(tag) {
        return Err(MaawError::InvalidTarget);
    }
    let removed_views_nodes = session.tags.remove(tag).unwrap();

    let mut visible_view_affected = false;

    // Tag removal from views. If there is not a given view, panic
    for view in &removed_views_nodes.views {
        seek_and_destroy_elem(session.views.get_mut(view).unwrap(), tag)?;
        if view == &session.current_view {
            visible_view_affected = true;
        }
    }

    // Tag removal from nodes. Signal orphaned nodes and now hidden ones.
    let mut orphaned_nodes: Vec<String> = Vec::new();
    let mut maybe_hidden_nodes: Vec<String> = Vec::new();
    for node in &removed_views_nodes.nodes {
        seek_and_destroy_elem(session.nodes.get_mut(node).unwrap(), tag)?;
        if session.nodes.get(node).unwrap().is_empty() {
            orphaned_nodes.push(node.to_string());
        } else {
            maybe_hidden_nodes.push(node.to_string());
        }
    }

    let mut effectively_hidden_nodes: Vec<String> = Vec::new();

    // We have hidden nodes only in the case the removed tag was in the visible one
    if visible_view_affected {
        let visible_view = session.views.get(&session.current_view).unwrap();
        let visible_view_tagset = hashset(visible_view);
        for node in &maybe_hidden_nodes {
            // Highly inefficient tag intersection search
            //let mut intersection_found = false;
            //for node_tag in session.nodes.get(node).unwrap() {
            //  for view_tag in visible_view {
            //      if view_tag == node_tag {
            //          intersection_found = true;
            //      }
            //  }
            //}
            let node_tagset = hashset(session.nodes.get(node).unwrap());
            if node_tagset.is_disjoint(&visible_view_tagset) {
                effectively_hidden_nodes.push(node.to_string());
            }
        }
    }

    //println!("Orphaned nodes: {:?}", orphaned_nodes);
    //println!("Hidden nodes: {:?}", effectively_hidden_nodes);

    // Here is the catch:
    // After a tag deletion, newly orphaned nodes MUST be visible.
    // So, if the removed tag belongs to the current view, these newly orphans
    // were previously visible, and they must stay so => they aren't affected.
    // If the tag isn't part of the current view, their state should change,
    // (from hidden to visible).
    // If the removed tag belongs to the current view, we must change
    // the state of non-orphans now-hidden nodes.
    let changed_state_nodes = if visible_view_affected {
        effectively_hidden_nodes
    } else {
        orphaned_nodes
    };

    Ok(MaawReturn {
        new_session: session,
        affected_nodes: Some(changed_state_nodes),
    })
}

fn maaw_tag_show_nodes(session: MaawData, tag: &str) -> result::Result<MaawReturn, MaawError> {
    if !session.tags.contains_key(tag) {
        return Err(MaawError::InvalidTarget);
    }
    let nodes = session.tags.get(tag).unwrap().nodes.to_vec();
    Ok(MaawReturn {
        new_session: session,
        affected_nodes: Some(nodes),
    })
}

fn maaw_tag_show_views(session: MaawData, tag: &str) -> result::Result<MaawReturn, MaawError> {
    if !session.tags.contains_key(tag) {
        return Err(MaawError::InvalidTarget);
    }
    let views = session.tags.get(tag).unwrap().views.to_vec();
    Ok(MaawReturn {
        new_session: session,
        affected_nodes: Some(views),
    })
}

fn maaw_view_add(mut session: MaawData, view: &str) -> result::Result<MaawReturn, MaawError> {
    if session.views.contains_key(view) {
        return Err(MaawError::TargetExists);
    }
    session.views.insert(view.to_string(), Vec::new());
    Ok(MaawReturn {
        new_session: session,
        affected_nodes: None,
    })
}

fn maaw_view_del(mut session: MaawData, view: &str) -> result::Result<MaawReturn, MaawError> {
    if !session.views.contains_key(view) {
        return Err(MaawError::InvalidTarget);
    }
    if session.views.len() == 1 {
        return Err(MaawError::WouldBeEmpty);
    }

    let mut changed_nodes: Option<Vec<String>> = None;

    if view == session.current_view {
        // Get another view
        let mut new_view = String::new();
        for some_view in session.views.keys() {
            if some_view != view {
                new_view = some_view.to_string();
                break;
            }
        }
        // Switch to it
        match maaw_view_set_current(session, view, &new_view) {
            Ok(maaw_ret) => {
                session = maaw_ret.new_session;
                changed_nodes = maaw_ret.affected_nodes;
            }
            Err(maaw_err) => return Err(maaw_err),
        };
    }

    let removed_view = session.views.remove(view).unwrap();
    // Clear up the view ref in the tags
    for tag in removed_view {
        seek_and_destroy_elem(&mut session.tags.get_mut(&tag).unwrap().views, view)?
    }

    Ok(MaawReturn {
        new_session: session,
        affected_nodes: changed_nodes,
    })
}

fn maaw_view_add_tag(
    mut session: MaawData,
    view: &str,
    tag: &str,
) -> result::Result<MaawReturn, MaawError> {
    if !session.tags.contains_key(tag) || !session.views.contains_key(view) {
        return Err(MaawError::InvalidTarget);
    }
    let working_view = session.views.get_mut(view).unwrap();

    let mut changed_nodes: Option<Vec<String>> = None;

    if view == session.current_view {
        let newtag_nodeset = hashset(&session.tags.get(tag).unwrap().nodes);
        let mut added_nodes: Vec<String> = Vec::new();

        let mut view_nodeset: HashSet<String> = HashSet::new();
        for current_tag in (&working_view).iter() {
            if current_tag == tag {
                //return Ok(MaawReturn{new_session: session, affected_nodes: None});
                return Err(MaawError::InvalidTarget);
            }
            view_nodeset = view_nodeset
                .union(&hashset(&session.tags.get(current_tag).unwrap().nodes))
                .cloned()
                .collect();
        }

        for new_visible_node in newtag_nodeset.difference(&view_nodeset) {
            added_nodes.push(new_visible_node.to_string());
        }
        changed_nodes = Some(added_nodes);
    }

    working_view.push(tag.to_string());
    session
        .tags
        .get_mut(tag)
        .unwrap()
        .views
        .push(view.to_string());

    Ok(MaawReturn {
        new_session: session,
        affected_nodes: changed_nodes,
    })
}

fn maaw_view_del_tag(
    mut session: MaawData,
    view: &str,
    tag: &str,
) -> result::Result<MaawReturn, MaawError> {
    if !session.tags.contains_key(tag) || !session.views.contains_key(view) {
        return Err(MaawError::InvalidTarget);
    }
    let working_view = session.views.get_mut(view).unwrap();

    seek_and_destroy_elem(working_view, &tag)?;
    seek_and_destroy_elem(&mut session.tags.get_mut(tag).unwrap().views, view)?;

    let changed_nodes: Option<Vec<String>> = if view == session.current_view {
        let deltag_nodeset = hashset(&session.tags.get(tag).unwrap().nodes);
        let mut removed_nodes: Vec<String> = Vec::new();

        let mut view_nodeset: HashSet<String> = HashSet::new();
        for tag in working_view {
            view_nodeset = view_nodeset
                .union(&hashset(&session.tags.get(tag).unwrap().nodes))
                .cloned()
                .collect();
        }

        for new_hidden_node in deltag_nodeset.difference(&view_nodeset) {
            removed_nodes.push(new_hidden_node.to_string());
        }
        Some(removed_nodes)
    } else {
        None
    };

    Ok(MaawReturn {
        new_session: session,
        affected_nodes: changed_nodes,
    })
}

fn maaw_view_toggle_tag(
    session: MaawData,
    view: &str,
    tag: &str,
) -> result::Result<MaawReturn, MaawError> {
    if !session.views.contains_key(view) || !session.tags.contains_key(tag) {
        return Err(MaawError::InvalidTarget);
    }
    if hashset(&session.views.get(view).unwrap()).contains(tag) {
        maaw_view_del_tag(session, view, tag)
    } else {
        maaw_view_add_tag(session, view, tag)
    }
}

fn maaw_view_move_to_tag(
    mut session: MaawData,
    view: &str,
    tag: &str,
) -> result::Result<MaawReturn, MaawError> {
    if !session.tags.contains_key(tag) || !session.views.contains_key(view) {
        return Err(MaawError::InvalidTarget);
    }
    let newtag_nodeset = hashset(&session.tags.get(tag).unwrap().nodes);
    let mut current_view_nodeset: HashSet<String> = HashSet::new();

    for current_tag in session.views.get(view).unwrap() {
        let current_tag_record = session.tags.get_mut(current_tag).unwrap();
        let current_tag_nodeset = hashset(&current_tag_record.nodes);
        current_view_nodeset = current_view_nodeset
            .union(&current_tag_nodeset)
            .cloned()
            .collect();
        seek_and_destroy_elem(&mut current_tag_record.views, view)?
    }
    let changed_nodes: Vec<String> = newtag_nodeset
        .symmetric_difference(&current_view_nodeset)
        .cloned()
        .collect();

    let changed_nodes: Option<Vec<String>> =
        if !changed_nodes.is_empty() && view == session.current_view {
            Some(changed_nodes)
        } else {
            None
        };

    session
        .views
        .insert(view.to_string(), vec![tag.to_string()]);
    session
        .tags
        .get_mut(tag)
        .unwrap()
        .views
        .push(view.to_string());

    Ok(MaawReturn {
        new_session: session,
        affected_nodes: changed_nodes,
    })
}

fn maaw_view_set_current(
    mut session: MaawData,
    oldview: &str,
    newview: &str,
) -> result::Result<MaawReturn, MaawError> {
    if !session.views.contains_key(oldview) || !session.views.contains_key(newview) {
        return Err(MaawError::InvalidTarget);
    }
    session.current_view = newview.to_string();
    // From the newly added tags and the now-removed tags, we must get the nodes which weren't (in)visible before.

    let oldview_tagset = hashset(session.views.get(oldview).unwrap());
    let newview_tagset = hashset(session.views.get(newview).unwrap());

    let mut changed_state_nodes: Vec<String> = Vec::new();

    // Search for nodes which should be hidden now
    let mut maybe_removed_nodeset: HashSet<String> = HashSet::new();
    for removed_tag in oldview_tagset.difference(&newview_tagset) {
        let tag_nodeset = hashset(&session.tags.get(removed_tag).unwrap().nodes);
        maybe_removed_nodeset = maybe_removed_nodeset.union(&tag_nodeset).cloned().collect();
    }
    for node in maybe_removed_nodeset {
        let node_tagset = hashset(session.nodes.get(&node).unwrap());
        if node_tagset.is_disjoint(&newview_tagset) {
            changed_state_nodes.push(node.to_string());
        }
    }

    // Search for nodes which should be visible now
    let mut maybe_added_nodeset: HashSet<String> = HashSet::new();
    for added_tag in newview_tagset.difference(&oldview_tagset) {
        let tag_nodeset = hashset(&session.tags.get(added_tag).unwrap().nodes);
        maybe_added_nodeset = maybe_added_nodeset.union(&tag_nodeset).cloned().collect();
    }
    for node in &maybe_added_nodeset {
        let node_tagset = hashset(&session.nodes.get(node).unwrap());
        if node_tagset.is_disjoint(&oldview_tagset) {
            changed_state_nodes.push(node.to_string());
        }
    }

    if changed_state_nodes.is_empty() {
        Ok(MaawReturn {
            new_session: session,
            affected_nodes: None,
        })
    } else {
        Ok(MaawReturn {
            new_session: session,
            affected_nodes: Some(changed_state_nodes),
        })
    }
}

fn maaw_view_get_current(session: MaawData) -> result::Result<MaawReturn, MaawError> {
    Ok(MaawReturn {
        affected_nodes: Some(vec![session.current_view.to_string()].to_vec()),
        new_session: session,
    })
}

fn maaw_view_show_tags(session: MaawData, view: &str) -> result::Result<MaawReturn, MaawError> {
    if !session.views.contains_key(view) {
        return Err(MaawError::InvalidTarget);
    }
    let tags = session.views.get(view).unwrap().to_vec();
    Ok(MaawReturn {
        new_session: session,
        affected_nodes: Some(tags),
    })
}

fn maaw_node_add(mut session: MaawData, node: &str) -> result::Result<MaawReturn, MaawError> {
    if session.nodes.contains_key(node) {
        return Err(MaawError::TargetExists);
    }
    session.nodes.insert(node.to_string(), Vec::new());
    Ok(MaawReturn {
        new_session: session,
        affected_nodes: None,
    })
}

fn maaw_node_del(mut session: MaawData, node: &str) -> result::Result<MaawReturn, MaawError> {
    if !session.nodes.contains_key(node) {
        return Err(MaawError::InvalidTarget);
    }
    let removed_node = session.nodes.remove(node).unwrap();
    for tag in removed_node {
        seek_and_destroy_elem(&mut session.tags.get_mut(&tag).unwrap().nodes, node)?
    }
    Ok(MaawReturn {
        new_session: session,
        affected_nodes: None,
    })
}

fn maaw_node_add_tag(
    mut session: MaawData,
    node: &str,
    tag: &str,
) -> result::Result<MaawReturn, MaawError> {
    if !session.nodes.contains_key(node) || !session.tags.contains_key(tag) {
        return Err(MaawError::InvalidTarget);
    }
    let visible_tagset = hashset(&session.views.get(&session.current_view).unwrap());
    let node_taglist = session.nodes.get_mut(node).unwrap();
    let previous_node_tagset = hashset(&node_taglist);
    let changed: Option<Vec<String>> = if !previous_node_tagset.is_empty()
        && previous_node_tagset.is_disjoint(&visible_tagset)
        && visible_tagset.contains(tag)
        // Was tagged and invisible, become visible
        || previous_node_tagset.is_empty()
        && !visible_tagset.contains(tag)
        // Was untagged and is now invisible
        {
        Some(vec![node.to_string()])
    } else {
        None
    };

    node_taglist.push(tag.to_string());
    session
        .tags
        .get_mut(tag)
        .unwrap()
        .nodes
        .push(node.to_string());

    Ok(MaawReturn {
        new_session: session,
        affected_nodes: changed,
    })
}

fn maaw_node_del_tag(
    mut session: MaawData,
    node: &str,
    tag: &str,
) -> result::Result<MaawReturn, MaawError> {
    if !session.nodes.contains_key(node) || !session.tags.contains_key(tag) {
        return Err(MaawError::InvalidTarget);
    }
    let visible_tagset = hashset(&session.views.get(&session.current_view).unwrap());
    let node_taglist = session.nodes.get_mut(node).unwrap();

    let previous_node_tagset = hashset(&node_taglist);

    let common_tagset: HashSet<String> = previous_node_tagset
        .intersection(&visible_tagset)
        .cloned()
        .collect();

    seek_and_destroy_elem(node_taglist, tag)?;
    seek_and_destroy_elem(&mut session.tags.get_mut(tag).unwrap().nodes, node)?;

    let was_visible_and_is_not_orphan =
        common_tagset.len() == 1 && common_tagset.contains(tag) && !node_taglist.is_empty();
    let is_invisible_and_is_now_orphan = common_tagset.is_empty() && node_taglist.is_empty();

    //println!("common_tagset={:?}", common_tagset);
    //println!("node_taglist={:?}", node_taglist);
    //println!("condition1={:?}", condition1);
    //println!("condition2={:?}", condition2);

    let now_hidden: Option<Vec<String>> =
        if was_visible_and_is_not_orphan || is_invisible_and_is_now_orphan {
            Some(vec![node.to_string()])
        } else {
            None
        };

    Ok(MaawReturn {
        new_session: session,
        affected_nodes: now_hidden,
    })
}

fn maaw_node_toggle_tag(
    session: MaawData,
    node: &str,
    tag: &str,
) -> result::Result<MaawReturn, MaawError> {
    if !session.nodes.contains_key(node) || !session.tags.contains_key(tag) {
        return Err(MaawError::InvalidTarget);
    }
    if hashset(&session.nodes.get(node).unwrap()).contains(tag) {
        maaw_node_del_tag(session, node, tag)
    } else {
        maaw_node_add_tag(session, node, tag)
    }
}

fn maaw_node_move_to_tag(
    mut session: MaawData,
    node: &str,
    tag: &str,
) -> result::Result<MaawReturn, MaawError> {
    if !session.nodes.contains_key(node) || !session.tags.contains_key(tag) {
        return Err(MaawError::InvalidTarget);
    }
    let visible_tagset = hashset(&session.views.get(&session.current_view).unwrap());
    let node_taglist = session.nodes.get(node).unwrap();

    let previous_node_tagset = hashset(&node_taglist);

    let was_visible =
        !(previous_node_tagset.is_disjoint(&visible_tagset)) || previous_node_tagset.is_empty();
    let is_now_visible = visible_tagset.contains(tag);

    for tag in previous_node_tagset.difference(&hashset(&[tag.to_string()])) {
        seek_and_destroy_elem(&mut session.tags.get_mut(tag).unwrap().nodes, node)?;
    }

    if !(previous_node_tagset.contains(tag)) {
        session
            .tags
            .get_mut(tag)
            .unwrap()
            .nodes
            .push(node.to_string());
    }

    session
        .nodes
        .insert(node.to_string(), vec![tag.to_string()]);
    Ok(MaawReturn {
        new_session: session,
        affected_nodes: if was_visible ^ is_now_visible {
            Some(vec![node.to_string()])
        } else {
            None
        },
    })
}

fn maaw_node_show_tags(session: MaawData, node: &str) -> result::Result<MaawReturn, MaawError> {
    let retrieved_node = session.nodes.get(node);
    match retrieved_node {
        Some(taglist) => Ok(MaawReturn {
            affected_nodes: Some(taglist.to_vec()),
            new_session: session,
        }),
        None => Err(MaawError::InvalidTarget),
    }
}

fn dispatch_argv(argv: &[String], session: MaawData) -> result::Result<MaawReturn, MaawError> {
    match argv[2].as_str() {
        "node" => match argv[4].as_str() {
            "add" => maaw_node_add(session, &argv[3]),
            "del" => maaw_node_del(session, &argv[3]),
            "showtags" => maaw_node_show_tags(session, &argv[3]),
            "addtag" => maaw_node_add_tag(session, &argv[3], &argv[5]),
            "deltag" => maaw_node_del_tag(session, &argv[3], &argv[5]),
            "movetotag" => maaw_node_move_to_tag(session, &argv[3], &argv[5]),
            "toggletag" => maaw_node_toggle_tag(session, &argv[3], &argv[5]),
            _ => Err(MaawError::InvalidCommand),
        },
        "tag" => match argv[4].as_str() {
            "add" => maaw_tag_add(session, &argv[3]),
            "del" => maaw_tag_del(session, &argv[3]),
            "shownodes" => maaw_tag_show_nodes(session, &argv[3]),
            "showviews" => maaw_tag_show_views(session, &argv[3]),
            _ => Err(MaawError::InvalidCommand),
        },
        "view" => {
            if argv[3].as_str() == "getcurrent" {
                maaw_view_get_current(session)
            } else {
                match argv[4].as_str() {
                    "add" => maaw_view_add(session, &argv[3]),
                    "del" => maaw_view_del(session, &argv[3]),
                    "showtags" => maaw_view_show_tags(session, &argv[3]),
                    "setcurrent" => maaw_view_set_current(session, &argv[3], &argv[5]),
                    "addtag" => maaw_view_add_tag(session, &argv[3], &argv[5]),
                    "deltag" => maaw_view_del_tag(session, &argv[3], &argv[5]),
                    "movetotag" => maaw_view_move_to_tag(session, &argv[3], &argv[5]),
                    "toggletag" => maaw_view_toggle_tag(session, &argv[3], &argv[5]),
                    _ => Err(MaawError::InvalidCommand),
                }
            }
        }
        _ => Err(MaawError::InvalidCommand),
    }
}

fn main() -> Result<(), MaawError> {
    let args: Vec<String> = env::args().collect();

    let filename = &args[1];

    let json_string = fs::read_to_string(filename).expect("Couldn't read file.");

    //println!("Raw string: {}", &json_string);

    let mut session = serde_json::from_str(&json_string)?;

    //println!("Maaw data: {:?}", session);

    // === OK ===
    //match maaw_tag_add(session, &String::from("t3")) {
    //  Ok(mr) => session = mr.new_session,
    //  Err(me) => panic!("During tag add: {:?}", me)
    //}

    // === OK ===
    //match maaw_view_add(session, &String::from("v3")) {
    //  Ok(mr) => session = mr.new_session,
    //  Err(me) => panic!("During view add: {:?}", me)
    //}

    // === OK ===
    //match maaw_node_add(session, &String::from("n4")) {
    //  Ok(mr) => session = mr.new_session,
    //  Err(me) => panic!("During node add: {:?}", me)
    //}

    // === OK ===
    //match maaw_view_del(session, &String::from("v1")) {
    //  Ok(mr) => { session = mr.new_session; println!("{:?}", mr.affected_nodes); },
    //  Err(me) => panic!("During node add: {:?}", me)
    //}

    // === OK ===
    //match maaw_tag_del(session, &String::from("t2")) {
    //  Ok(mr) => { session = mr.new_session; println!("Affected nodes: {:?}", mr.affected_nodes); },
    //  Err(me) => panic!("During tag del: {:?}", me)
    //}

    // === OK ===
    //match maaw_view_add_tag(session, &String::from("v2"), &String::from("t1")) {
    //  Ok(mr) => { session = mr.new_session; println!("Affected nodes: {:?}", mr.affected_nodes); },
    //  Err(me) => panic!("During tag del: {:?}", me)
    //}

    // === OK ===
    //match maaw_view_del_tag(session, &String::from("v1"), &String::from("t1")) {
    //  Ok(mr) => { session = mr.new_session; println!("Affected nodes: {:?}", mr.affected_nodes); },
    //  Err(me) => panic!("During tag del: {:?}", me)
    //}

    // === OK ===
    //match maaw_view_move_to_tag(session, &String::from("v1"), &String::from("t1")) {
    //  Ok(mr) => { session = mr.new_session; println!("Affected nodes: {:?}", mr.affected_nodes); },
    //  Err(me) => panic!("During tag del: {:?}", me)
    //}

    // === OK ===
    //match maaw_node_add_tag(session, &String::from("node2"), &String::from("t1")) {
    //  Ok(mr) => { session = mr.new_session; println!("Affected nodes: {:?}", mr.affected_nodes); },
    //  Err(me) => panic!("During tag add: {:?}", me)
    //}
    //match maaw_node_del_tag(session, &String::from("node2"), &String::from("t1")) {
    //  Ok(mr) => { session = mr.new_session; println!("Affected nodes: {:?}", mr.affected_nodes); },
    //  Err(me) => panic!("During tag del: {:?}", me)
    //}
    //match maaw_node_del_tag(session, &String::from("node1"), &String::from("t1")) {
    //  Ok(mr) => { session = mr.new_session; println!("Affected nodes: {:?}", mr.affected_nodes); },
    //  Err(me) => panic!("During tag del: {:?}", me)
    //}
    //match maaw_node_del_tag(session, &String::from("node2"), &String::from("t2")) {
    //  Ok(mr) => { session = mr.new_session; println!("Affected nodes: {:?}", mr.affected_nodes); },
    //  Err(me) => panic!("During tag del: {:?}", me)
    //}

    // === OK ===
    //match maaw_node_toggle_tag(session, &String::from("node2"), &String::from("t1")) {
    //  Ok(mr) => { session = mr.new_session; println!("Affected nodes: {:?}", mr.affected_nodes); },
    //  Err(me) => panic!("During tag toggle: {:?}", me)
    //}
    //match maaw_node_toggle_tag(session, &String::from("node2"), &String::from("t1")) {
    //  Ok(mr) => { session = mr.new_session; println!("Affected nodes: {:?}", mr.affected_nodes); },
    //  Err(me) => panic!("During tag toggle: {:?}", me)
    //}

    // === OK ===
    //match maaw_node_move_to_tag(session, &String::from("node2"), &String::from("t1")) {
    //  Ok(mr) => { session = mr.new_session; println!("Affected nodes: {:?}", mr.affected_nodes); },
    //  Err(me) => panic!("During tag move: {:?}", me)
    //}
    //match maaw_node_move_to_tag(session, &String::from("node1"), &String::from("t2")) {
    //  Ok(mr) => { session = mr.new_session; println!("Affected nodes: {:?}", mr.affected_nodes); },
    //  Err(me) => panic!("During tag move: {:?}", me)
    //}

    let cmd_ret = dispatch_argv(&args, session);
    match cmd_ret {
        Ok(maaw_ret) => {
            session = maaw_ret.new_session;
            if let Some(list) = maaw_ret.affected_nodes {
                for elem in &list {
                    eprintln!("{}", elem);
                    Command::new("bspc")
                        .arg("node")
                        .arg(elem)
                        .arg("-g")
                        .arg("hidden")
                        .output()
                        .expect("failed to execute bspc");
                }
            }
            // Could be streamlined using serde file serializer.
            let serialized_session = serde_json::to_string(&session)?;
            fs::write(filename, serialized_session);
        }
        Err(maaw_error) => eprintln!("{:?}", maaw_error),
    }

    //println!("Serialized maaw data: {}", serialized_session);

    Ok(())
}
