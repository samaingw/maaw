Roadmap:
- [ ] Move all `maaw_*` functions into `MaawState`
- [ ] Switch to a daemon architecture and remove the JSON support/`serde`
  dependencies
- [ ] Use references in `MaawState` to enforce correctness and reduce memory allocations
- [ ] Automatically subscribe to and call to `bspc`
- [ ] Ditch `bspc` and interact directly with `bspwm` via its socket
